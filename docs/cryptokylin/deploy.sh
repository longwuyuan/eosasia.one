

gcloud container clusters create eos \
    --cluster-version=1.9.7-gke.3 \
    --enable-ip-alias \
    --num-nodes 1 \
    --machine-type n1-highmem-4

kubectl create namespace kylin

# not used because it's massively complicated to have private cluster
# gcloud beta container clusters create eos \
# 	--cluster-version=1.9.7-gke.3 \
# 	--private-cluster \
# 	--master-ipv4-cidr 172.16.0.16/28 \
# 	--enable-ip-alias \
# 	--num-nodes 1 \
# 	--tags private-node \
# 	--machine-type n1-highmem-4 \
# 	--create-subnetwork ""

# gcloud beta container clusters update eos \
# 	    --enable-master-authorized-networks \
# 	        --master-authorized-networks 0.0.0.0/0

# not using secret anymore
# kubectl create secret generic genesis-conf --from-file=genesis.json=genesis.json -n kylin
# kubectl create secret generic fullnode-conf --from-file=config.ini=config-fullnode.ini -n kylin
# kubectl create secret generic bp-conf --from-file=config.ini=config-bp.ini -n kylin

kubectl create configmap genesis-conf --from-file=genesis.json=genesis.json -n "$K8S_NS"
kubectl create configmap fullnode-conf --from-file=config.ini=config-fullnode.ini -n "$K8S_NS"
kubectl create configmap bp-conf --from-file=config.ini=config-bp.ini -n "$K8S_NS"
kubectl create secret generic bp-key --from-literal=key=$'private-key = ["PUBLIC_KEY","PRIVATE_KEY"]\n' -n kylin

kubectl apply -f nodeos-fullnode-svc-lb.yaml -n kylin
kubectl apply -f nodeos-fullnode-svc.yaml -n kylin
kubectl apply -f nodeos-fullnode.yaml -n kylin
kubectl apply -f nodeos-bp-svc.yaml -n kylin
kubectl apply -f nodeos-bp-pvc.yaml -n kylin
kubectl apply -f nodeos-bp.yaml -n kylin

kubectl scale statefulset nodeos-bp-master --replicas=1 -n kylin