# EOS monitoring

- eoskeeper url https://github.com/eosstore/eoskeeper/blob/master/README‐EN.md
- Put eoskeeper inside the nodeos pod
  - Create customized nodeos image with eoskeeper inside. Refer Dockerfile for same
- Deployed a influxdb via helm but need to initiate with a env-var caled __INFLUXDB_DB__ to create the db used by eoskeeper. Avoid auth of any kind for now.

-
