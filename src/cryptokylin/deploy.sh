gcloud container clusters create eos \
    --cluster-version=1.9.7-gke.3 \
    --enable-ip-alias \
    --num-nodes 1 \
    --machine-type n1-highmem-4

kubectl create namespace kylin
kubectl create namespace kylin-api

# not used because it's massively complicated to have private cluster
# gcloud beta container clusters create eos \
# 	--cluster-version=1.9.7-gke.3 \
# 	--private-cluster \
# 	--master-ipv4-cidr 172.16.0.16/28 \
# 	--enable-ip-alias \
# 	--num-nodes 1 \
# 	--tags private-node \
# 	--machine-type n1-highmem-4 \
# 	--create-subnetwork ""

# gcloud beta container clusters update eos \
# 	    --enable-master-authorized-networks \
# 	        --master-authorized-networks 0.0.0.0/0

# not using secret anymore
# kubectl create secret generic genesis-conf --from-file=genesis.json=genesis.json -n kylin
# kubectl create secret generic fullnode-conf --from-file=config.ini=config-fullnode.ini -n kylin
# kubectl create secret generic bp-conf --from-file=config.ini=config-bp.ini -n kylin

cd configmaps

kubectl create configmap genesis-conf --from-file=genesis.json=genesis.json -n kylin
kubectl create configmap fullnode-conf --from-file=config.ini=config-fullnode.ini -n kylin
kubectl create configmap bp-conf --from-file=config.ini=config-bp.ini -n kylin
#kubectl create secret generic bp-key --from-literal=key=$'private-key = ["EOS8LpSDbAPACxHJoxJsbWdZ7pvEeZpZ9qZfKiEiC6KuF6btUiwgZ","5JncoWn5gQd2Qite8sLUiSFGB7LLgeHdNVxLXT65wESmtnHE19S"]\n' -n kylin

cd ../storage
kubectl apply -f data-nodeos-fullnode-0-pvc.yml -n kylin
kubectl apply -f data-nodeos-api-0-pvc.yml -n kylin-api
kubectl apply -f data-nodeos-api-1-pvc.yml -n kylin-api
kubectl apply -f nodeos-bp-pvc.yml -n kylin

cd ../workloads
kubectl apply -f nodeos-fullnode.yml -n kylin
kubectl apply -f nodeos-bp-master.yml -n kylin

cd ../services
kubectl apply -f nodeos-fullnode-svc.yaml -n kylin
kubectl apply -f nodeos-bp-svc.yml -n kylin
kubectl apply -f nodeos-api-svc-lb-nskylin.yml -n kylin
kubectl apply -f nodeos-api-svc-lb-nskylinapi.yml -n kylin-api

kubectl apply -f nodeos-api.yml

kubectl scale statefulset nodeos-bp-master --replicas=1 -n kylin
