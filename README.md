# EOS monitoring

EOS is a blockchain described here https://eos.io/

The "block-producer"  and the "fullnode" are two types of containerized blockchain applications.

The EOS software image is here https://hub.docker.com/r/eoscanada/eos/ .

One software to monitor metrics of EOS is called eoskeeper.

## Deploy influxdb

  - eoskeeper sends metrics to influxdb out of the box. Better get influxdb running before eoskeeper runs.

  - Install influxdb ;

  ```
  helm install stable/influxdb --name eoskeeperdb --namespace kylin
  ```

- Make influxdb create a database called 'eosdb'.
- The default database name referred to in the eoskeeper config.ini is "eosdb". Edit the infludb deployment to create a database by that name. ```The name here is hardcoded for now in the eoskeeper config. It will get exposed to configure on the fly, in later editions of this solution ``` ;

  ```
  kubectl edit deploy/eoskeeperdb-influxdb -n kylin
  ```

- The dockerhub influxdb image exposes a variable to set the name of the database created by default.

- Insert the environment variable into the influxdb deployment. These entries go below the "image" spec ;

  ```
  env:
  - name: INFLUXDB_DB
    value: eosdb  
  ```

- Save and exit. If there are no errors, then K8s will deploy new container of the influxdb pod.

- Exec into the influxdb container ;

  ```
  kubectl exec -ti <influxdbpod-name> -n kylin bash
  ```

- Start the influxdb client ;

  ```
  influx
  ```

- Check if database called eosdb was created ;

  ```
  show databases
  ```

  - TODO: Automate & persist config + data.



## Deploy eoskeeper

  - eoskeeper url https://github.com/eosstore/eoskeeper/blob/master/README‐EN.md .

  - It is a single python script.

  - eoskeeper polls port http://127.0.0.1:8888 on the eos container.

  - One way to get it working is described below (this is still work in progress) ;

    - Install python, python-pip, vim & lsof in the containers of the pods created using the image canadaeos/eos:v1.0.7 ;

      ```
      # Get the names of the pods
      kubectl get po -n kylin

      # Exec into the pod
      kubectl exec -ti <fullnode or bp-node name> -n kylin bash

      # Update os software repo (bionic in this case)
      apt-get -y update

      # Install the requires software
      apt-get install -y python python-pip vim lsof
    ```

    - pip install sh & requests in the same image.

      ```
      pip install sh requests

      ```

    - If not exist, then, create the /data directory and optionally the eosio.log file referred to for eoskeeper logs ;

      ```
      mkdir /data
      touch /data/eosio.log
      ```

    - Copy/paste eoskeeper.py from the eoskeeper repo, to /usr/local/bin/ inside the image, now that vim is installed there.

    - Copy config.ini from the same repo, inside the image image to the path /etc/eoskeeper.

    - ___Change the "role" parameter, as documented in eoskeeper readme.___

    - __The eoskeeper needs to run inside all the containers of bp pod as well as the fullnode pod.__


  - Another way is to create the bp-node pods and the fullnode pods using this image https://hub.docker.com/r/longwuyuan/eos-with-eoskeeper/,

  - The image above has eoskeeper installed inside it. Refer its Docerfile for details.

  - `But` eoskeeper is not (yet) configured to run automatically. (This is work-in-progress.)

  - So exec into the fullnode/bp container.

    ```
    kubectl exec -ti <podname> -n <namespace> bash
    ```

  - Execute eoskeeper script and push it to the background.

    ```
    /usr/local/bin/eoskeeper &
    ```

  - Watch the stdout of eoskeeper script.

    - ``` Running "ps -uxa" will show a process "tail -f /data/eosio.log" ```. That is done by eoskeeper.


  - If all goes well, influxdb will get the measurements timeseries data.

  - Exec into the influxdb pod again .

  - Start influx client and use schema/data commands to confirm measurements are coming through ;

    ```
    show databases

    use database eosdb

    show measurements

    show series

    show tag keys

    show field keys

    select * from eos_full_node_info limit 3

    ```

    - TODO: Automate eoskeeper availability for containers created from eos image.

    - TODO: Automation Could be a pip package or a go executable.

    - TODO: K8s version v1.11 has a feature to use shared-namespaces between containers. Can leverage that to exec eoskeeper.py .

    - TODO: Using shared-namespace for executing needs to be tested for providing python runtime.

    - TODO: Automate init of eoskeeper at container boot. Solutions like supervisord or monit.

    - TODO: Automate a single entrypoint or command for both EOS and EOSKEEPER.


## Deploy grafana

- Install grafana ;

  ```
  helm install stable/grafana -n kylin
  ```

- The chart creates a service type of "ClusterIP" by default.

- We don't want to bother with an ingress for now. If implemented with ingress, we save costs. LB is expensive on clouds. But this is a TODO.

- Change the service type for grafana to LoadBalancer.

  ```
  kubectl edit svc/grafana -n kylin
  ```

- Check and get the external IP-Address for the LoadBalancer created for grafana UI

  ```
  kubectl get svc -n kylin
  ```

- Point your browser to the external IP-Address. Default grafana username is admin.

- Grafana sets a default password for the admin user.

- The output of the helm install command shows how to get the default password for user admin from the related secret in k8s ;

  ```
  kubectl get secret --namespace kylin grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
  ```

- Get the password and login as admin.


- Configure a datasource with below specs ;

  ```
  Name                              =  `Pick A Name`

  Type                              =   influxdb

  HTTP

  URL                               =   http://eoskeeperdb-influxdb:8086

  Access                            =   Server(Default)

  Skip TLS Verification (Insecure)  =   {Checkbox as checked}

  Advanced HTTP settings

  Whitelisted Cookies                   {Leave it at whatever default}

  InfluxDB Details

  Database                          =   eosdb
  User                              =   {blank/empty}
  password                          =   {blank/empty}

  ```


- Set this datasource as Default.

- Create a new dashboard.

- Add a panel of type graph.

- Choose datasource default.

- Each configurable paramter will drop down the relevant data from the schema.

  - The field hbn is block-height as per eoskeeper readme.

  - The field linkn is number of links as per eoskeeper readme.

- TODO: Automate & persist config + data.

- TODO: Pagerduty, Slack and other integrations are just a matter of clicking a few buttons in Grafana so need to complete that once infra stablizes or the zero value links field will send infinite loop notifications.
